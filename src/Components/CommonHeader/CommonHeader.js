import React from 'react';
import Logo from './codeforces_logo.png';
import styles from './CommonHeader.module.scss';
import { withRouter } from 'react-router-dom';
import { navBarItems } from '../../utils/constants';
import { Button } from '@material-ui/core';
import  { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    root1 : {
        width: '100px',
        borderRadius : '6px',
        fontSize : '14px',
        fontWeight : 'bold',
        background: 'yellow',
    },
    root2: {
        width: '100px',
        borderRadius : '6px',
        fontSize : '14px',
        fontWeight : 'bold',
        background: 'blue',
    },
    root3: {
        width: '110px',
        borderRadius : '6px',
        fontSize : '14px',
        fontWeight : 'bold',
        background: 'red',
    },
});

const CommonHeader = (props) =>{
    const classes = useStyles();
    const navbaritems = navBarItems.map( item => ({
            ...item,
            cls: classes[`root${item.id}`]
        
    }));
    const _onClick = (destination) => {
        props.history.replace(destination);
    }
     return (
         <div>
             <div className={styles.headerContainer}>
              <div className={styles.logoWrapper}>
               <img src={Logo} alt="codeforces" className={styles.logoWidth} />
              </div>
              <div className={styles.heading}>
                  Get latest updates of Contests, diverse problemset and stalk your friends with the new Comptetetive Stalker
              </div>
             </div>
             <div className={styles.buttonWrapper}>
                {navbaritems.map(item => (
                    <div key={item.id} className={styles.buttonDiv}>
                        <Button className={item.cls} onClick={() => _onClick(item.destination)}>{item.text}</Button>
                    </div>
                ))}
             </div>
         </div>
     )
};

export default withRouter(CommonHeader);