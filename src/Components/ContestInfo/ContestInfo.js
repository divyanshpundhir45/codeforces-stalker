import React from 'react'

import styles from './ContestInfo.module.scss';

const ContestInfo = (props) => {
    console.log(props);
    const {
        name,
        standing,
    } = props;
    const problems = props.problems.map(problem => {
        if(problem.tags.length<2)
        return problem;
        else{
            const tags = problem.tags.splice(0,2);
            const obj = {
                ...problem,
                tags : tags,
            };
            return obj;
        }
    })
    return (
        <div>
            <div className={styles.nameWrapper}>
              {name}
            </div>
            <div className={styles.problemOuterDiv}>
                {problems.map((problem,index) =>(
                    <div key={index} className={styles.singleProblem}>
                        <div>
                            {problem.problemName}
                         </div>
                         <div>
                             rating - {problem.rating}
                         </div>
                        {problem.tags ? (
                            <div className={styles.tagsSection}>
                              <div>
                                tags - 
                              </div>
                               <div style={{display : 'flex'}}>
                                  {problem.tags.map((tag,index) =>{
                                     let d=tag;
                                     if(index !== (problem.tags.length-1)){
                                        d = d+ ',';
                                    }
                                    return(
                                      <div>
                                        {d}
                                      </div>
                                    );
                                    })}
                                </div>
                            </div>
                        ) : null}
                    </div>
                ))}
            </div>
            <div className={styles.standingText}>
                Standings of the round
            </div>
            <div className={styles.standingOuterDiv}>
                <div>
                    {standing.map(standing => (
                        <div className={styles.singleStanding} key={standing.rank}>
                            <div>
                                {standing.rank}{'.'}
                            </div>
                            <div style={{marginRight : '5px'}}>
                               {' '}{standing.handle}
                            </div>
                            <div>
                              Points - {standing.points}
                            </div>
                        </div>
                    ))}
                </div>

            </div>
        </div>
    );
};

export default ContestInfo;
