import React from 'react';
import Card from '../../widget/Card/Card';
import { Button } from '@material-ui/core';
import styles from './ContestPerformanceCard.module.scss';
import { makeStyles } from '@material-ui/core/styles';
import { colorobj } from '../../utils/constants';

const useStyles = makeStyles({
    root : {
        width : '50px',
        borderRadius : '6px',
    }
});

const ContestPerformanceCard = (props) => {
    
    const classes = useStyles();
    const {
        name,
        rank,
        newRating,
        oldRating
    } = props;
    const delta = newRating - oldRating;
    const objcolor1 = colorobj(oldRating);
    const objcolor2 = colorobj(newRating);
    return (
        <Card className={styles.cardStyle}>
             <div className={styles.name}>
                 {name}
             </div>
             <div className={styles.rank}>
                Contest Rank-{rank}
             </div>
             <div className={styles.ratingWrapper}> 
                <div style={{...objcolor1,width : '50px',display : 'flex',justifyContent :'center'}}>
                  {oldRating}
                </div>
                <div>
                     {'->'}
                </div>
                <div style={{...objcolor2,width : '50px',display : 'flex',justifyContent :'center'}}>
                 {newRating}
                </div>
             </div>
        </Card>
    );
};

export default ContestPerformanceCard;
