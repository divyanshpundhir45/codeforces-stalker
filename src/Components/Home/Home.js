import React from 'react';

import styles from './Home.module.scss';

const Home = (props) =>{
    return (
        <div>
            <div className={styles.CodeforcesIntro}>
                About Codeforces
            </div>
            <div className={styles.codeforcesDesc}>
            Codeforces is a website that hosts competitive programming contests.It is maintained by a group of competitive programmers from ITMO University led by Mikhail Mirzayanov.Since 2013, Codeforces claims to surpass Topcoder in terms of active contestants.As of 2018, it has over 600,000 registered users.Codeforces along with other similar websites are used by top sport programmers like Gennady Korotkevich, Petr Mitrichev, Benjamin Qi and Makoto Soejima, and by other programmers interested in furthering their careers.
            </div>
            <div className={styles.CodeforcesIntro}>
                About Us
            </div>
            <div className={styles.codeforcesDesc}>
                We wish to provide all details and info regarding Codeforces ranging from 
                contests , contest questions,contest standings, problemsets, user information, user rating 
                and much more. 
            </div>
        </div>
    )
};

export default Home;