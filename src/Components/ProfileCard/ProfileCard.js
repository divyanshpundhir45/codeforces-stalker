import React from 'react'

import Card from '../../widget/Card/Card';

import { colorobj } from '../../utils/constants';

import namepng from './name.png';
import handlepng from './handle.png';
import locationpng from './location.png';
import maxrating from './maxrating.png';
import maxrank from './maxrank.png';

import styles from './ProfileCard.module.scss';

const ProfileCard = ({details}) => {
    const {
        firstName = '',
        lastName = '',
        city = '',
        country='',
        handle,
        contribution,
        maxRank,
        maxRating,
        rank,
        rating,
        organisation = '',
        friendOfCount='',
    } = details;

    let name = '';
    let location ='';
    if(city.length || country.length)
    location = `${city},${country}`

    if(firstName.length || lastName.length)
    name=`${firstName} ${lastName}`;

    const getFieldSection = (title,name,path) =>(
        <div className={styles.sectionWrapper}>
            <div className={styles.iconWrapper}>
                <img src={path} alt="name" className={styles.iconWidth} />
            </div>
            <div>
              {title} - {name}
            </div>
        </div>
    );
    
    const getMaxRankSection = (title,maxRank, maxRating) =>{
        const obj = colorobj(maxRating);
        return (
            <div className={styles.sectionWrapper}>
                <div className={styles.iconWrapper}>
                    <img src={maxrank} alt="max-rank" className={styles.iconWidth}/>
                </div>
                <div>
                    {title} - <span style={{...obj,padding : '4px',borderRadius : '5px'}}>{maxRank}</span>
                </div>
            </div>
        );
    }
    return (
        <Card className={styles.cardStyle}>
             <div className={styles.heading}>
                 Profile Card
             </div>
             <div>
                 {name ? getFieldSection('UserName',name,namepng) : null}
             </div>
             <div>
                 {getFieldSection('Handle',handle,handlepng)}
             </div>
             <div>
                 {location ? getFieldSection('Location',location,locationpng) : null}
             </div>
             <div>
                 {getFieldSection('Max Rating',maxRating,maxrating)}
             </div>
             <div>
                 {getMaxRankSection('Max Rank',maxRank,maxRating)}
             </div>
             <div>
                 {getFieldSection('Curr Rating',rating,maxrating)}
             </div>
             <div>
                 {getMaxRankSection('Curr Rank',rank,rating)}
             </div>
        </Card>
    )
}

export default ProfileCard;
