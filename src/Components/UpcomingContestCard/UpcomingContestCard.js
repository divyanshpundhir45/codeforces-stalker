import React from 'react'
import Card from '../../widget/Card/Card';

import { convertTime } from '../../utils/constants';

import styles from './UpcomingContestCard.module.scss';

const UpcomingContestCard = ({contest}) => {
    const {
        name,
        id,
        phase,
        durationSeconds,
    } = contest;
    const duration = convertTime(durationSeconds);
    return (
        <Card className={styles.cardStyling}>
            <div>
              <div>
              Name - {name}
              </div>
              <div>
              Contest Number - {id}
              </div>
              <div>
              Contest Status - {phase}
              </div>
              <div>
              Contest Duration - {duration}
              </div>
            </div>
        </Card>
    );
};

export default UpcomingContestCard;
