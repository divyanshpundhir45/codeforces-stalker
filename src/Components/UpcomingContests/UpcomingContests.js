import React , {useState, useEffect} from 'react'
import axios from 'axios';
import { 
    currentTime,
    key,
    secret,
    apiSig,
    hashFunction
} from '../../utils/constants';

import UpcomingContestCard from '../UpcomingContestCard/UpcomingContestCard';

import { Button, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import styles from './UpcomingContests.module.scss';


const useStyles = makeStyles({
    root : {
        width : '150px',
        backgroundColor : 'blue',
        borderRadius : '6px'
    },
})

const UpcomingContests = () => {

    const classes = useStyles();
    const [display , setDisplay] = useState(false);
    const [contestDetails,setContestDetails] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if(display && !contestDetails.length){
            setLoading(true);
            const hash = hashFunction(`${apiSig}/contest.list?apiKey=${key}&time=${currentTime}#${secret}`);
            const requrl = `https://codeforces.com/api/contest.list?apiKey=${key}&time=${currentTime}&apiSig=${apiSig}${hash}`;
            axios.get(requrl)
              .then(response => {
                  const arr= response.data.result.splice(0,6);
                  setContestDetails([...arr]);
                  console.log([...arr]);
                  setLoading(false);
              })
              .catch(() => setLoading(false));
        }
    },[display]);

    const toggleDisplay = () => {
        const val=display;
        setDisplay(!val);
    }

    const getUpcomingContestSection = () => (
        <div>
            {contestDetails.map(contest =>(
                <div key={contest.id}>
                    <UpcomingContestCard contest={contest} />
                </div>
            ))}
        </div>
    );

    return (
        <div className={styles.outerDiv}>
            <Button onClick={toggleDisplay} className={classes.root}>Upcoming Contests</Button>
        
        { loading ? <CircularProgress /> : null}
        {display && !loading ? getUpcomingContestSection() : null}
        </div>
    );
}

export default UpcomingContests;
