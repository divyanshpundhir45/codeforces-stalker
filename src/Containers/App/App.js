import React from 'react';
import Home from '../../Components/Home/Home';
import CommonHeader from '../../Components/CommonHeader/CommonHeader';
import Users from '../../Containers/Users/Users';
import Contests from '../../Containers/Contests/Contests';

import {HashRouter as Router, Route , Switch} from 'react-router-dom';



function App(props) {
  return (
     <Router>
         <CommonHeader />
         <Switch>
             <Route path='/users' component={Users} />
             <Route path='/contests' component={Contests} />
             <Route exact path='/' component={Home} />
         </Switch>
     </Router>
  );
}

export default App;
