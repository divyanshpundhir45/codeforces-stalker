import React , {useState} from 'react';
import axios from 'axios';
import { Button , CircularProgress, TextField} from '@material-ui/core';
import ContestInfo from '../../Components/ContestInfo/ContestInfo';
import { makeStyles } from '@material-ui/core/styles';
import styles from './ContestRatingChange.module.scss';

const useStyles = makeStyles({
    root : {
        backgroundColor : 'yellow',
        color : 'black',
        '&:hover' : {
            backgroundColor : 'yellow'
        }
    }
})

const ContestRatingChange = () => {

    const classes = useStyles();
    const [display , setDisplay] = useState(false);
    const [error, setError]  = useState(false);
    const [search , setSearch] = useState('');
    const [loading ,setLoading] = useState(false);
    const [clicked, setClicked] = useState(false);

    const [standings , setStandings] = useState([]);
    const [name , setName] = useState('');
    const [problems, setProblems] = useState([]);

    const onCallApiClick = () =>{
        setClicked(true);
        setError(false);
        setLoading(true);
        const requrl=`https://codeforces.com/api/contest.standings?contestId=${search}&from=1&count=5&showUnofficial=false`;
        axios.get(requrl)
        .then(response => {
            const data = response.data.result;
            setName(data.contest.name);
            const standing = data.rows.map( r => {
                const handle = r.party.members[0].handle;
                const points = r.points;
                const rank = r.rank;
                return {handle : handle,points : points,rank : rank};
            });
            setStandings(standing);
            const problems = data.problems.map ( p =>{
                const problemName = `${p.index}.${p.name}`
                const rating = p.rating;
                const tags = p.tags;
                return {problemName : problemName, rating : rating , tags : tags};
            })
            setProblems(problems);
            setLoading(false);
        })
        .catch((error) => { 
            setError(true); 
            setLoading(false);
        });
    };

    const getContestDetails = () => (
        <ContestInfo problems={problems} standing={standings} name={name} />
    );
    const getClickedDetails = () =>(
        <>
        <div>
          {loading ? <CircularProgress/> : null}
        </div>
        <div>
          {error ? <div> Contest Id is not valid. </div> : null}
        </div>
        <div>
         {!loading && !error ? getContestDetails() : null}
        </div>
        </>
    );
    const getSearchSection = () =>{
        return (
            <div className={styles.search}>
                <div>
                    <div>
                    Input the Contest Id
                    </div>
                    <div>
                        <TextField value={search} onChange={(event)=> setSearch(event.target.value)} />
                    </div>
                </div>
                <div>
                    <Button onClick={onCallApiClick}> Search </Button>
                </div>
               {clicked ? getClickedDetails() : null}
            </div>
        )
    };

    const _displayClicked = () => {
        const val=display;
        setDisplay(!val);
        if(val){
            setSearch('');
            setError(false);
            setClicked(false);
        }
    }
    return (
        <div className={styles.rootDiv}>
                <Button className={classes.root} onClick={_displayClicked}>
                   Past Contest Details
                </Button>

                {display ? getSearchSection() : null}
        </div>
    );
};

export default ContestRatingChange;
