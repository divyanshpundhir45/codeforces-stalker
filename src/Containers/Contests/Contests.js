import React from 'react';

import UpcomingContests from '../../Components/UpcomingContests/UpcomingContests';
import ContestRatingChange from '../../Containers/ContestRatingChange/ContestRatingChange';

import styles from './Contests.module.scss';


const Contests = (props) => {
    return(
        <div>
            <div className={styles.heading}>
              Welcome to the Contest Section
            </div>
            <div className={styles.contentWrapper}>
                <div style={{width : '33%'}}>
                  <UpcomingContests />
                </div>
                <div style={{width : '33%'}}>
                  <ContestRatingChange />
                </div>
            </div>
        </div>
    );
};

export default Contests;