import React , {useState } from 'react';
import axios from 'axios';
import ProfileCard from '../../Components/ProfileCard/ProfileCard';
import search from './search.png';
import  { TextField , Button , CircularProgress } from '@material-ui/core';
import { 
    currentTime,
    key,
    secret,
    apiSig,
    hashFunction
} from '../../utils/constants';
import { makeStyles } from '@material-ui/core/styles';
import styles from './SearchComponent.module.scss';


const useStyle = makeStyles({
    root : {
        borderRadius : '6px',
        background : 'red',
        width : '100px'
    }
});

const SearchComponent = (props) => {

    const classes = useStyle();
    const [handle,setHandle] = useState('');
    const [loading, setLoading] = useState(false);
    const [details,setDetails] = useState({});
    const [first, setFirst] = useState(true);
    const [error,setError] = useState(false);
    const onSubmit = () =>{
        if(first){
           setFirst(false);
        }
        setLoading(true);
        let hash = hashFunction(`${apiSig}/user.info?apiKey=${key}&handles=${handle}&time=${currentTime}#${secret}`);
        let requrl = `https://codeforces.com/api/user.info?handles=${handle}&apiKey=${key}&time=${currentTime}&apiSig=${apiSig}${hash}`;
        axios.get(requrl)
         .then( response =>{
             const obj= response.data.result[0];
             setDetails({...obj});
             setLoading(false);
             setError(false);
         }).catch( error => {
             setError(true);
             setLoading(false);
         });
    }
    return (
        <div className={styles.outerDiv}>
            <div style={{fontSize : '20px',marginBottom : '20px'}}>
                Search the Handle of your friend.
            </div>
            <div>
                <TextField value={handle} onChange={(event) => setHandle(event.target.value)} />
            </div>
            <div className={styles.lowerDiv}>
                <div className={styles.iconWrapper}>
                  <img src={search} alt="search" className={styles.imgWidth} />
                </div>
                <div>
                  <Button onClick={onSubmit} className={classes.root}>Search</Button>
                </div>
            </div>

            <div>
                { !error && !loading && !first ? 
                  <ProfileCard details={details} /> : null }
                

                {loading ? <CircularProgress /> : null }

                {error ? <div className={styles.error}> Sorry couldn't find the handle</div> : null}
            
            </div>
        </div>
    )
};

export default SearchComponent;