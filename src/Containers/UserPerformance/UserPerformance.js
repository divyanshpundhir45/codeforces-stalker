import React , {useState , useEffect} from 'react';
import axios from 'axios';
import {TextField , Button, CircularProgress} from '@material-ui/core';

import { 
    currentTime,
    key,
    secret,
    apiSig,
    hashFunction
} from '../../utils/constants';

import ContestPerformanceCard from '../../Components/ContestPerformanceCard/ContestPerformanceCard';
import search from '../SearchComponent/search.png';
import { makeStyles } from '@material-ui/core/styles';

import styles from './UserPerformance.module.scss';

const useStyle = makeStyles({
    root : {
        borderRadius : '6px',
        background : 'blue',
        width : '100px'
    }
});

const UserPerformance = () =>{
    const classes = useStyle();
    const [handle,setHandle] = useState('');
    const [ details, setDetails] = useState([]);
    const [ error , setError] = useState(false);
    const [ loading , setLoading] = useState(false);
    const onSubmit = () => {
        setLoading(true);
        setError(false);
        let hash = hashFunction(`${apiSig}/user.rating?apiKey=${key}&handle=${handle}&time=${currentTime}#${secret}`);
        let requrl = `https://codeforces.com/api/user.rating?handle=${handle}&apiKey=${key}&time=${currentTime}&apiSig=${apiSig}${hash}`;
        axios.get(requrl)
         .then( response => {
            console.log('response',response);
            const c= response.data.result;
            const data= c.reverse();
            setDetails(data);
            setLoading(false);
         })
         .catch(()=>{
             setError(true);
             setLoading(false);
         })
    }

    const getPerformanceDetail = () =>{
        const detail = details.splice(0,6);
        return (
            <div>
                {detail.map(d =>(
                    <ContestPerformanceCard
                      key={d.contestId}
                      name={d.contestName}
                      rank={d.rank}
                      newRating={d.newRating}
                      oldRating={d.oldRating}
                    />
                ))}
            </div>
        )
    }
    return(
        <div className={styles.outerDiv}>
            <div style={{fontSize : '20px',marginBottom : '20px'}}>
                Search handle to view the performance
            </div>
            <div>
                <TextField value={handle} onChange={(event) => setHandle(event.target.value)} />
            </div>
            <div className={styles.lowerDiv}>
                <div className={styles.iconWrapper}>
                  <img src={search} alt="search" className={styles.imgWidth} />
                </div>
                <div>
                  <Button onClick={onSubmit} className={classes.root}>Search</Button>
                </div>
            </div>
            <div>
                <div>
                    {loading ? <CircularProgress /> : null}
                </div>
                <div>
                    {error ? <div>Sorry no handle found</div> : null}
                </div>
                <div>
                    {!loading && !error &&  details.length ? getPerformanceDetail() : null}
                </div>
            </div>
        </div>
    );
};

export default UserPerformance;