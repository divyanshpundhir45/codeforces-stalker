import React from 'react';

import { Route } from 'react-router-dom';
import SearchComponent from '../SearchComponent/SearchComponent';
import { Button } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import styles from './Users.module.scss';
import UserPerformance from '../UserPerformance/UserPerformance';


const useStyles = makeStyles({
    root : {
        backgroundColor : 'black',
        borderRadius : '6px',
        color : 'white',
        '&:hover' : {
            backgroundColor : 'black',
        }
    }
});


const Users = (props) => {
    const classes = useStyles();

    const _onClick = (newloc) => {
        const newurl = props.match.url + newloc;
        props.history.push(newurl);
    }
    return (
        <div>
            <div className={styles.heading}>
              Welcome to the Users Section. 
            </div>
            <div className={styles.buttonWrapper}>
                <div style={{marginRight : '10px'}}>
                    <Button className={classes.root} onClick={()=> _onClick('/search')}>
                        Search User Handle
                    </Button>
                </div>
                <div>
                    <Button className={classes.root} onClick={() => _onClick('/performance')}>
                        View User performance
                    </Button>
                </div>
            </div>
            <Route path={`${props.match.url}/search`} exact component={SearchComponent} />
            <Route path={`${props.match.url}/performance`} exact component={UserPerformance} />
        </div>
    )
};

export default Users;