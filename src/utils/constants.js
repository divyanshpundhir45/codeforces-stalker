import crypto from 'crypto';

export const hashFunction= (str) => (crypto.createHash('sha512').update(str).digest('hex'))
export const currentTime = Math.round((new Date()).getTime() / 1000);
export const key= '40c04ef48c34410d440d38f110a18a8e4d336811';
export const secret= 'c72a3555754bfd0c3b479ccc4e64ace5af757b8c';
export const apiSig = Math.floor((Math.random()+1)*100000);  
export const navBarItems = [
    {
        id : 1,
        text : 'USERS',
        destination : '/users'
    },
    {
        id: 2,
        text : 'CONTESTS',
        destination : '/contests',
    },
    {
        id: 3,
        text: 'PROBLEMSET',
        destination : '/problemset'
    },
];


export const colorobj = (rating) =>{
    if(rating>=0 && rating<=1199)
    return {backgroundColor : 'grey'};

    if(rating>=1200 && rating<=1399)
    return {backgroundColor : 'green'};


    if(rating>=1400 && rating<=1599)
    return {backgroundColor : 'skyblue'};

    if(rating>=1600 && rating<=1899)
    return {backgroundColor : 'blue'};

    if(rating>=1900 && rating<=2299)
    return {backgroundColor : 'orange'};

    return {backgroundColor : 'red'};
    
}


export const convertTime = (seconds) =>{
    const hours = Math.floor(seconds/3600);
    const timeleft = seconds - 3600*hours;
    const minutes = Math.floor(timeleft/60);
    const h=hours.toString();
    let m= minutes.toString();
    if(m.length===1)
    m = '0' +m;
    return `${h}h:${m}m`;
}