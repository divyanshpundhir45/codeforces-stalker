import React from "react";
import Card from "../Card/Card";
import CircularProgress from "../CircularProgress";
import styles from "./Loader.module.scss";

const Loader = () => {
  return (
    <Card className={styles.cardStyling}>
      <CircularProgress />
    </Card>
  );
};
export default Loader;
